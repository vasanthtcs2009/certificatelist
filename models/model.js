const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const certSchema = mongoose.Schema({
   cert_name:{
    type: String,
    required: true
  },
  cert_serial:{
    type: String,
    required: true
  },
  cert_type:{
    type:String,
    enum:['dp_cert','iib_cert','external_system_cert','appliance_cert','dns_cert','host_cert'],
    required:true
  },
  environment:{
    type:String,
    enum:['rdev','mdev','msit','rsit','ruat','muat','prdp','prod'],
    required:true
  },
  cert_zone:{
    type:String,
    enum:['internal','external'],
    required:true
  },
  start_date:{
    type:Date,
    required:true
  },
  expiry_date:{
    type:Date,
    required:true
  },
  cert_owner:{
    type:String,
    required:true
  },
  installed_system:{
    type:String,
    required:false
  },
  sr_number:{
    type:String,
    required:false
  }
});

const Certificate = module.exports = mongoose.model('Certificate',certSchema);
