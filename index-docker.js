//importing modules
const express = require('express');
const cors = require('cors');
const bodyparser = require('body-parser');
const path = require('path');
const mongoose = require('mongoose');
const fs = require('fs');
const https = require('https');

//defining a variable for express
var index = express();

//importing routing file
var route = require('./routes/route');

// loading the key and certificate files for SSL
/*const options = {
  key: fs.readFileSync("./certificatelist.pem"),
  cert: fs.readFileSync("./certificatelist.crt")
};*/

//connecting to mongodb
mongoose.connect('mongodb://mongo:27017/certificatelist');

//successful connection to mongodb
mongoose.connection.on('connected',()=>{
  console.log('successfully connected to mongodb on port 27017');
});

//error connecting to mongodb
mongoose.connection.on('error',(err)=>{
  if(err)
  {
    console.log('Error connecting to database due to'+err);
  }
});

//define port
const port = 2102;

//adding middleware
index.use(cors());

//adding body parser support for parsing json
index.use(bodyparser.json());

//defining routes
index.use('/api',route);

//defining default routes
index.use('/',(req,res)=>{
    res.send({
        responseCode: '0000',
        responseMessage: 'Success',
        responseDescription:'Test Page, Application Boot successful!!'
    });
})

//start the app on importing
index.listen(port,()=>{
  console.log('Port started successfully on : '+port);
})

//start the app on importing
/*
https.createServer(options,index).listen(port,()=>{
  console.log('Port started successfully on : '+port);
})
*/
