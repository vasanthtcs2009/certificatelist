import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Certificate } from './certificate';
import 'rxjs/add/operator/map';

@Injectable()
export class CertificateService {

	constructor(private http: Http) { }

	//retrieving certs
	getCerts() {
		return this.http.get('http://localhost:2102/api/certs')
			.map(res => res.json());
	}

	//add contacts
	addCert(newCert) {
		var headers = new Headers();
		headers.append('Content/Type', 'application/json');
		return this.http.post('http://localhost:2102/api/cert', newCert, { headers: headers })
			.map(res => res.json());
	}

	//delete method
	deleteCert(id) {
		return this.http.delete('http://localhost:2102/api/cert/' + id)
			.map(res => res.json());
	}

}
