import { Component, OnInit } from '@angular/core';
import { CertificateService } from '../certificate.service';
import { Certificate } from '../certificate';

@Component({
  selector: 'app-certs',
  templateUrl: './certificate.component.html',
  styleUrls: ['./certificate.component.css'],
  providers: [CertificateService]
})
export class CertificateComponent implements OnInit {
  certs: Certificate[];
  cert: Certificate;
  cert_name: string;
  cert_serial: string;
  cert_type: string;
  environment: string;
  cert_zone: string;
  start_date: string;
  expiry_date: string;
  cert_owner: string;
  installed_system: string;
  sr_number: string

  constructor(private certService: CertificateService) { }

  addCert() {
    const newCert = {
      cert_name: this.cert_name,
      cert_serial: this.cert_serial,
      cert_type: this.cert_type,
      environment: this.environment,
      cert_zone: this.cert_zone,
      start_date: this.start_date,
      expiry_date: this.expiry_date,
      cert_owner: this.cert_owner,
      installed_system: this.installed_system,      
      sr_number: this.sr_number
    }
    this.certService.addCert(newCert)
      .subscribe(cert => {
        this.certs.push(cert);
        this.certService.getCerts()
          .subscribe(certs =>
            this.certs = certs);
      });
  }

  deleteCert(id: any) {
    var certs = this.certs;
    this.certService.deleteCert(id)
      .subscribe(data => {
        if (data.n == 1) {
          for (var i = 0; i < certs.length; i++) {
            if (certs[i]._id == id) {
              certs.splice(i, 1);
            }
          }
        }
      })
  }

  ngOnInit() {
    this.certService.getCerts()
      .subscribe(certs =>
        this.certs = certs);
  }

}
