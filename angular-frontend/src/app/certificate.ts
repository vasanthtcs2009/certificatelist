export class Certificate {
  _id?: string;
  cert_name: string;
  cert_serial: string;
  cert_type: string;
  environment: string;
  cert_zone: string;
  start_date: string;
  expiry_date: string;
  cert_owner: string;
  installed_system: string;
  sr_number: string
}
