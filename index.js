//importing modules
const express = require('express');
const cors = require('cors');
const bodyparser = require('body-parser');
const path = require('path');
const mongoose = require('mongoose');
require('dotenv').config();

//defining a variable for express
var index = express();

//importing routing file
var route = require('./routes/route');

//define the port and ip address variables
var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 2102,
    ip   = process.env.IP   || process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';

//define mongodb connection parameters
var mongoUser =  process.env.MONGODB_USER,
    mongoDatabase = process.env.MONGODB_DATABASE,
    mongoPassword = process.env.MONGODB_PASSWORD,
    mongoHost = process.env.MONGODB_SERVICE_HOST,
    mongoPort = process.env.MONGODB_SERVICE_PORT,
    mongoURL = 'mongodb://';

mongoURL += mongoUser + ':' + mongoPassword + '@';
mongoURL += mongoHost + ':' +  mongoPort + '/' + mongoDatabase;

//connecting to mongodb
mongoose.connect(mongoURL);

//successful connection to mongodb
mongoose.connection.on('connected',()=>{
  console.log('Successfully connected to mongodb on port 27017');
});

//error connecting to mongodb
mongoose.connection.on('error',(err)=>{
  if(err)
  {
    console.log('Error connecting to database due to'+err);
  }
});

//adding middleware
index.use(cors());

//adding body parser support for parsing json
index.use(bodyparser.json());

//defining routes
index.use('/api',route);

//defining default routes
index.use('/',(req,res)=>{
    res.send({
        responseCode: '0000',
        responseMessage: 'Success',
        responseDescription:'Test Page, Application Boot successful!!'
    });
})

//start the app on importing
index.listen(port,ip,()=>{
  console.log('Service started successfully on : '+ip+':'+port);
})
