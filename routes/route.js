const express = require('express');
const router = express.Router();

const Certificate = require('../models/model');

//GET method
router.get('/cert/:id',(req,res,next)=>{
  Certificate.findOne({_id: req.params.id},function(err,certificate){
    if(err)
    {
        res.status(404).json({
            responseCode: '9999',
            responseMessage: 'Failure',
            responseDescription: 'Requested content not found'
        });
    }
    else
    {
        res.json(certificate);      
    }
  });
})

//GET method to return the count of items in the collection
router.get('/count',(req,res)=>{
	Certificate.collection.count((err,count)=>{
		if(err)
		{
			res.status(500).json({
                responseCode: '9999',
                responseMessage: 'Failure',
                responseDescription: err.message
            });
		}
		else
		{
		    res.status(200).json({
                responseCode: '0000',
                responseMessage: 'Success',
                responseDescription: 'Record Count is ==> '+count				
            });
		}
	})
	
});

//GET method
router.get('/certs', (req, res, next) => {
    Certificate.collection.count(function (err, count) {
        if (count === 0) {
            res.status(404).json({
                responseCode: '9999',
                responseMessage: 'Failure',
                responseDescription: 'Collection is empty'
            });
        }
        else {
            Certificate
            .find()
            .sort({expiry_date : 1})
            .exec(function (err, certs) { res.json(certs) });
        }
    });
});

//POST method
router.post('/cert', (req, res, next) => {
    var start_date = req.body.start_date;
    var expiry_date = req.body.expiry_date;
    let newCertificate = new Certificate({
        cert_name: req.body.cert_name,
        cert_serial: req.body.cert_serial,
        cert_type: req.body.cert_type,
        environment: req.body.environment,
        cert_zone: req.body.cert_zone,
        start_date: start_date,
        expiry_date: expiry_date,
        cert_owner: req.body.cert_owner,
        installed_system: req.body.installed_system,
        sr_number: req.body.sr_number
    });

    newCertificate.save((err, certificate) => {       
            if (err) {
                res.status(500).json({
                    responseCode: '9999',
                    responseMessage: 'Failure',
                    responseDescription: 'Failed to insert data due to ' + err
                });
            }
            else {
            res.status(201).json({
                responseCode: '0000',
                responseMessage: 'Success',
                responseDescription: 'Data inserted successfully'
            });
        }
    });
});

//DELETE method for all data
router.delete('/certs/all', (req, res, next) => {
    Certificate.remove({}, function (err) {
        if (err) {
            res.status(500).send({
                responseCode: '9999',
                responseMessage: 'Failure',
                responseDescription: err.message
            });
        }
        else {
            res.status(202).send({
                responseCode: '0000',
                responseMessage: 'Success',
                responseDescription: 'All records deleted successfully' });
        }
    });
});

//DELETE method for particular id
router.delete('/cert/:id', (req, res, next) => {
    Certificate.findOneAndRemove({ _id: req.params.id }, function (err) {
        if (err) {
            res.status(500).send({
                responseCode: '9999',
                responseMessage: 'Failure',
                responseDescription: 'Incorrect identifier,can\'t find record to delete'
            });
        }
        else {
            res.status(202).send({
                responseCode: '0000',
                responseMessage: 'Success',
                responseDescription: 'Record deleted successfully' });
        }
    });
});

//default method to handle all other incorrect requests
router.all('*', (req, res) => {
    res.status(404).send({
        responseCode: '9999',
        responseMessage: 'Failure',
        responseDescription: 'Requested Resource could not be found'
    });
})


//export the modules
module.exports = router;
